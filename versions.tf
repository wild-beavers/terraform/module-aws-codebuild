terraform {
  required_version = ">= 0.13.0"

  required_providers {
    aws = {
      source                = "hashicorp/aws"
      version               = ">= 5"
      configuration_aliases = [aws.secrets_manager_replica]
    }
    random = {
      source  = "hashicorp/random"
      version = ">= 2.1"
    }
  }
}
