output "project_name" {
  description = "Project name"
  value       = join("", [for project in aws_codebuild_project.default : project.name])
}

output "project_id" {
  description = "Project ID"
  value       = join("", [for project in aws_codebuild_project.default : project.id])
}

output "role_id" {
  description = "IAM Role ID"
  value       = join("", [for iam_role in aws_iam_role.default : iam_role.id])

}

output "role_arn" {
  description = "IAM Role ARN"
  value       = join("", [for iam_role in aws_iam_role.default : iam_role.arn])
}

output "cache_bucket_name" {
  description = "Cache S3 bucket name"
  value       = module.this.enabled && local.s3_cache_enabled ? local.cache_bucket_name_normalised : "UNSET"
}

output "cache_bucket_arn" {
  description = "Cache S3 bucket ARN"
  value       = module.this.enabled && local.s3_cache_enabled ? join("", [for cache_bucket in module.cache_bucket : cache_bucket.arn]) : "UNSET"
}

output "badge_url" {
  description = "The URL of the build badge when badge_enabled is enabled"
  value       = join("", [for project in aws_codebuild_project.default : project.badge_url])
}

output "project_arn" {
  description = "Project ARN"
  value       = join("", [for project in aws_codebuild_project.default : project.arn])
}
