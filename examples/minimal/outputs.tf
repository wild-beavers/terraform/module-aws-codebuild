output "project_name" {
  description = "Project name"
  value       = module.minimal.project_name
}

output "project_id" {
  description = "Project ID"
  value       = module.minimal.project_id
}

output "role_id" {
  description = "IAM Role ID"
  value       = module.minimal.role_id
}

output "role_arn" {
  description = "IAM Role ARN"
  value       = module.minimal.role_arn
}

output "cache_bucket_name" {
  description = "Cache S3 bucket name"
  value       = module.minimal.cache_bucket_name
}

output "cache_bucket_arn" {
  description = "Cache S3 bucket ARN"
  value       = module.minimal.cache_bucket_arn
}

output "badge_url" {
  description = "The URL of the build badge when badge_enabled is enabled"
  value       = module.minimal.badge_url
}
