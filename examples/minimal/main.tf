locals {
  prefix = "${random_string.start_letter.result}${random_string.this.result}"
}

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

module "minimal" {
  source = "../../"

  description            = "This is my awesome CodeBuild project"
  concurrent_build_limit = 10
  name                   = "${local.prefix}tftest"
  iam_policy_name        = "${local.prefix}tftest"
  iam_role_name          = "${local.prefix}tftest"

  iam_pass_roles = ["arn:aws:iam::aws:policy/AdministratorAccess"]

  providers = {
    aws.secrets_manager_replica = aws
  }
}
