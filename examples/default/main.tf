locals {
  prefix = "${random_string.start_letter.result}${random_string.this.result}"
}

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

resource "aws_ecr_repository" "ecr_repo" {
  name                 = "${local.prefix}tftestecr"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }
}

module "default" {
  source = "../.."

  namespace = "feature"
  stage     = "tests"
  name      = "${local.prefix}tftest"

  build_image        = "aws/codebuild/standard:2.0"
  build_compute_type = "BUILD_GENERAL1_SMALL"
  build_timeout      = 60
  privileged_mode    = false

  iam_policy_name = "${local.prefix}tftest"
  iam_role_name   = "${local.prefix}tftest"

  image_repo_name = aws_ecr_repository.ecr_repo.name
  image_tag       = "latest"

  extra_permissions = ["ec2:*"]

  environment_variables = [
    {
      name  = "FOO"
      value = "bar"
      type  = "PLAINTEXT"
    }
  ]

  artifact_type   = "CODEPIPELINE"
  source_type     = "CODEPIPELINE"
  source_location = "s3"

  cache_expiration_days       = 7
  cache_bucket_suffix_enabled = true
  cache_type                  = "NO_CACHE"

  badge_enabled = false

  providers = {
    aws.secrets_manager_replica = aws.us2
  }
}
