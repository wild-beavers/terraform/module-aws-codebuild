output "project_name" {
  value = module.default.project_name
}

output "project_id" {
  value = module.default.project_id
}

output "role_id" {
  value = module.default.role_id
}

output "role_arn" {
  value = module.default.role_arn
}

output "cache_bucket_name" {
  value = module.default.cache_bucket_name
}

output "cache_bucket_arn" {
  value = module.default.cache_bucket_arn
}

output "badge_url" {
  value = module.default.badge_url
}
