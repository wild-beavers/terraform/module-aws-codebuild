module "disable" {
  source = "../../"

  enabled = false

  providers = {
    aws.secrets_manager_replica = aws
  }
}
