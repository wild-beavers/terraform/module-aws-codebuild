## 5.0.0

- feat: (BREAKING) delete `source_credential_server_type` variable
- maintenance: migrate module to Terraform registry
- chore: bump pre-commit hooks
- maintenance: add `versions.tf` into all examples
- refactor: replace splat operators by square bracket syntax
- refactor: add variable types in `variables.tf`
- refactor: add `nullable = false` into multiple variables

## 4.0.0

- feat: (BREAKING) isolate `iam:passrole` action on codebuild baseline IAM policy. To enable `iam:passrole`, whitelist roles by using `var.iam_pass_roles`
- test: makes tests pass with Wildbeaver ecosystem

## 3.0.0

- feat: (BREAKING) remove the auth. This remove `var.private_repository`, `var.source_credential_auth_type`, `var.source_credential_token`, `var.source_credential_user_name`, and `var.secondary_sources`
- tech: (BREAKING) set the AWS provider to 5+
- test: add gitlab-ci file

## 2.0.0

- doc: starts `CHANGELOG.md`
- refactor: Minimally Wildbeaver-ise project
- maintenance: updates secret manager public module to `1`+
- feat: requires `secrets_manager_replica` provider alias to be set
