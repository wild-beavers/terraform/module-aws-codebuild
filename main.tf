data "aws_caller_identity" "default" {
}

data "aws_region" "default" {
}

module "cache_bucket" {
  source = "git::https://gitlab.com/wild-beavers/terraform/module-aws-bucket-s3.git?ref=6.0.0"

  count = module.this.enabled && local.s3_cache_enabled ? 1 : 0

  name = local.cache_bucket_name_normalised

  iam_policy_create       = false
  iam_policy_output_jsons = true

  force_destroy = true
  tags          = var.tags

  sse_enabled = var.encryption_enabled

  versioning_configuration = var.versioning_enabled ? {
    status = "Enabled"
  } : null

  logging_configuration = var.access_log_bucket_name != "" ? {
    target_bucket = var.access_log_bucket_name
    target_prefix = "logs/${module.this.id}/"
  } : null

  lifecycle_configuration_rules = [{
    id     = "codebuildcache"
    status = "Enabled"

    filter = {
      prefix = "/"
    }

    expiration = {
      days = var.cache_expiration_days
    }
  }]
}

resource "random_string" "bucket_prefix" {
  count   = module.this.enabled ? 1 : 0
  length  = 12
  numeric = false
  upper   = false
  special = false
  lower   = true
}

locals {
  cache_bucket_name = "${module.this.id}${var.cache_bucket_suffix_enabled ? "-${join("", [for bucket_prefix in random_string.bucket_prefix : bucket_prefix.result])}" : ""}"

  ## Clean up the bucket name to use only hyphens, and trim its length to 63 characters.
  ## As per https://docs.aws.amazon.com/AmazonS3/latest/dev/BucketRestrictions.html
  cache_bucket_name_normalised = substr(
    join("-", split("_", lower(local.cache_bucket_name))),
    0,
    min(length(local.cache_bucket_name), 63),
  )

  s3_cache_enabled = var.cache_type == "S3"

  ## This is the magic where a map of a list of maps is generated
  ## and used to conditionally add the cache bucket option to the
  ## aws_codebuild_project
  cache_options = {
    "S3" = {
      type     = "S3"
      location = module.this.enabled && local.s3_cache_enabled ? join("", [for cache_bucket in module.cache_bucket : cache_bucket.id]) : "none"
    },
    "LOCAL" = {
      type  = "LOCAL"
      modes = var.local_cache_modes
    },
    "NO_CACHE" = {
      type = "NO_CACHE"
    }
  }

  # Final Map Selected from above
  cache = local.cache_options[var.cache_type]
}

resource "aws_iam_role" "default" {
  count                 = module.this.enabled ? 1 : 0
  name                  = var.iam_role_name
  assume_role_policy    = data.aws_iam_policy_document.role.json
  force_detach_policies = true
  tags                  = var.tags
}

data "aws_iam_policy_document" "role" {
  statement {
    sid = ""

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["codebuild.amazonaws.com"]
    }

    effect = "Allow"
  }
}

resource "aws_iam_policy" "default" {
  count  = module.this.enabled ? 1 : 0
  name   = var.iam_policy_name
  path   = "/service-role/"
  policy = data.aws_iam_policy_document.combined_permissions.json
  tags   = var.tags
}

resource "aws_iam_policy" "default_cache_bucket" {
  count = module.this.enabled && local.s3_cache_enabled ? 1 : 0


  name   = "${module.this.id}-cache-bucket"
  path   = "/service-role/"
  policy = join("", [for permissions_cache_bucket in data.aws_iam_policy_document.permissions_cache_bucket : permissions_cache_bucket.json])
}

data "aws_s3_bucket" "secondary_artifact" {
  count  = module.this.enabled ? (var.secondary_artifact_location != null ? 1 : 0) : 0
  bucket = var.secondary_artifact_location
}

data "aws_iam_policy_document" "permissions" {
  count = module.this.enabled ? 1 : 0

  statement {
    sid = "baseline"

    actions = compact(concat([
      "codecommit:GitPull",
      "ecr:BatchCheckLayerAvailability",
      "ecr:CompleteLayerUpload",
      "ecr:GetAuthorizationToken",
      "ecr:InitiateLayerUpload",
      "ecr:PutImage",
      "ecr:UploadLayerPart",
      "ecs:RunTask",
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "ssm:GetParameters",
    ], var.extra_permissions))

    effect = "Allow"

    resources = [
      "*",
    ]
  }

  dynamic "statement" {
    for_each = length(var.iam_pass_roles) > 0 ? { 0 = 0 } : {}

    content {
      sid = "PassRole"

      actions = ["iam:PassRole"]

      effect = "Allow"

      resources = var.iam_pass_roles
    }
  }

  dynamic "statement" {
    for_each = var.use_registry_credentials ? [1] : []

    content {
      sid = "SecretManager"

      actions = [
        "secretsmanager:GetSecretValue",
      ]

      effect = "Allow"

      resources = [
        local.registry_secret_arn
      ]
    }
  }

  dynamic "statement" {
    for_each = var.secondary_artifact_location != null ? [1] : []
    content {
      sid = ""

      actions = [
        "s3:PutObject",
        "s3:GetBucketAcl",
        "s3:GetBucketLocation"
      ]

      effect = "Allow"

      resources = [
        join("", [for secondary_artifact in data.aws_s3_bucket.secondary_artifact : secondary_artifact.arn]),
        "${join("", [for secondary_artifact in data.aws_s3_bucket.secondary_artifact : secondary_artifact.arn])}/*",
      ]
    }
  }
}

data "aws_iam_policy_document" "vpc_permissions" {
  count = module.this.enabled && var.vpc_config != {} ? 1 : 0

  statement {
    sid = ""

    actions = [
      "ec2:CreateNetworkInterface",
      "ec2:DescribeDhcpOptions",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DeleteNetworkInterface",
      "ec2:DescribeSubnets",
      "ec2:DescribeSecurityGroups",
      "ec2:DescribeVpcs"
    ]

    resources = [
      "*",
    ]
  }

  statement {
    sid = ""

    actions = [
      "ec2:CreateNetworkInterfacePermission"
    ]

    resources = [
      "arn:aws:ec2:${var.aws_region}:${var.aws_account_id}:network-interface/*"
    ]

    condition {
      test     = "StringEquals"
      variable = "ec2:Subnet"
      values = formatlist(
        "arn:aws:ec2:${var.aws_region}:${var.aws_account_id}:subnet/%s",
        var.vpc_config.subnets
      )
    }

    condition {
      test     = "StringEquals"
      variable = "ec2:AuthorizedService"
      values = [
        "codebuild.amazonaws.com"
      ]
    }

  }
}

data "aws_iam_policy_document" "combined_permissions" {
  override_policy_documents = compact([
    join("", [for permissions in data.aws_iam_policy_document.permissions : permissions.json]),
    var.vpc_config != {} ? join("", [for vpc_permissions in data.aws_iam_policy_document.vpc_permissions : vpc_permissions.json]) : null
  ])
}

data "aws_iam_policy_document" "permissions_cache_bucket" {
  count = module.this.enabled && local.s3_cache_enabled ? 1 : 0
  statement {
    sid = ""

    actions = [
      "s3:*",
    ]

    effect = "Allow"

    resources = [
      join("", [for cache_bucket in module.cache_bucket : cache_bucket.arn]),
      "${join("", [for cache_bucket in module.cache_bucket : cache_bucket.arn])}/*",
    ]
  }
}

resource "aws_iam_role_policy_attachment" "default" {
  count      = module.this.enabled ? 1 : 0
  policy_arn = join("", [for iam_policy in aws_iam_policy.default : iam_policy.arn])
  role       = join("", [for iam_role in aws_iam_role.default : iam_role.id])
}

resource "aws_iam_role_policy_attachment" "default_cache_bucket" {
  count      = module.this.enabled && local.s3_cache_enabled ? 1 : 0
  policy_arn = join("", [for default_cache_bucket in aws_iam_policy.default_cache_bucket : default_cache_bucket.arn])
  role       = join("", [for iam_role in aws_iam_role.default : iam_role.id])
}

locals {
  registry_secret_arn = var.use_registry_credentials ? (
    var.external_registry_credential_secret_arn != null ? var.external_registry_credential_secret_arn : module.registry_credentials[0].secrets["registry"].arn
  ) : ""
}

module "registry_credentials" {
  source = "git::https://gitlab.com/wild-beavers/terraform/module-aws-secret-manager.git?ref=1"

  count = var.use_registry_credentials ? 1 : 0

  secrets = {
    registry = {
      name                    = var.registry_credential_secret_name
      description             = "Secret used to login to registry for the ${var.name} CodeBuild project."
      recovery_window_in_days = var.registry_credential_secret_recovery_window_in_days
      policy_scope            = "NONE"
      secrets = {
        username = var.registry_credential_secret_username
        password = var.registry_credential_secret_password
      }
    }
  }

  kms_key_id = var.registry_credential_secret_kms_key_id

  tags = var.tags

  providers = {
    aws.replica = aws.secrets_manager_replica
  }
}

resource "aws_codebuild_project" "default" {
  count = module.this.enabled ? 1 : 0

  name                   = module.this.id
  description            = var.description
  concurrent_build_limit = var.concurrent_build_limit
  service_role           = join("", [for iam_role in aws_iam_role.default : iam_role.arn])
  badge_enabled          = var.badge_enabled
  build_timeout          = var.build_timeout
  source_version         = var.source_version != "" ? var.source_version : null
  tags = {
    for name, value in var.tags :
    name => value
    if length(value) > 0
  }

  artifacts {
    type     = var.artifact_type
    location = var.artifact_location
  }

  # Since the output type is restricted to S3 by the provider (this appears to
  # be an bug in AWS, rather than an architectural decision; see this issue for
  # discussion: https://github.com/hashicorp/terraform-provider-aws/pull/9652),
  # this cannot be a CodePipeline output. Otherwise, _all_ of the artifacts
  # would need to be secondary if there were more than one. For reference, see
  # https://docs.aws.amazon.com/codepipeline/latest/userguide/action-reference-CodeBuild.html#action-reference-CodeBuild-config.
  dynamic "secondary_artifacts" {
    for_each = var.secondary_artifact_location != null ? [1] : []
    content {
      type                = "S3"
      location            = var.secondary_artifact_location
      artifact_identifier = var.secondary_artifact_identifier
      encryption_disabled = !var.secondary_artifact_encryption_enabled
      # According to AWS documention, in order to have the artifacts written
      # to the root of the bucket, the 'namespace_type' should be 'NONE'
      # (which is the default), 'name' should be '/', and 'path' should be
      # empty. For reference, see https://docs.aws.amazon.com/codebuild/latest/APIReference/API_ProjectArtifacts.html.
      # However, I was unable to get this to deploy to the root of the bucket
      # unless path was also set to '/'.
      path = "/"
      name = "/"
    }
  }

  cache {
    type     = lookup(local.cache, "type", null)
    location = lookup(local.cache, "location", null)
    modes    = lookup(local.cache, "modes", null)
  }

  environment {
    compute_type                = var.build_compute_type
    image                       = var.build_image
    type                        = var.build_type
    privileged_mode             = var.privileged_mode
    image_pull_credentials_type = var.use_registry_credentials ? "SERVICE_ROLE" : "CODEBUILD"

    dynamic "registry_credential" {
      for_each = var.use_registry_credentials ? [1] : []

      content {
        credential          = local.registry_secret_arn
        credential_provider = "SECRETS_MANAGER"
      }
    }

    environment_variable {
      name  = "AWS_REGION"
      value = signum(length(var.aws_region)) == 1 ? var.aws_region : data.aws_region.default.name
    }

    environment_variable {
      name  = "AWS_ACCOUNT_ID"
      value = signum(length(var.aws_account_id)) == 1 ? var.aws_account_id : data.aws_caller_identity.default.account_id
    }

    dynamic "environment_variable" {
      for_each = signum(length(var.image_repo_name)) == 1 ? [""] : []
      content {
        name  = "IMAGE_REPO_NAME"
        value = var.image_repo_name
      }
    }

    dynamic "environment_variable" {
      for_each = signum(length(var.image_tag)) == 1 ? [""] : []
      content {
        name  = "IMAGE_TAG"
        value = var.image_tag
      }
    }

    dynamic "environment_variable" {
      for_each = signum(length(module.this.stage)) == 1 ? [""] : []
      content {
        name  = "STAGE"
        value = module.this.stage
      }
    }

    dynamic "environment_variable" {
      for_each = signum(length(var.github_token)) == 1 ? [""] : []
      content {
        name  = "GITHUB_TOKEN"
        value = var.github_token
      }
    }

    dynamic "environment_variable" {
      for_each = var.environment_variables
      content {
        name  = environment_variable.value.name
        value = environment_variable.value.value
        type  = environment_variable.value.type
      }
    }

  }

  source {
    buildspec           = var.buildspec
    type                = var.source_type
    location            = var.source_location
    report_build_status = var.report_build_status
    git_clone_depth     = var.git_clone_depth != null ? var.git_clone_depth : null

    dynamic "git_submodules_config" {
      for_each = var.fetch_git_submodules ? [""] : []
      content {
        fetch_submodules = true
      }
    }
  }

  dynamic "vpc_config" {
    for_each = length(var.vpc_config) > 0 ? [""] : []
    content {
      vpc_id             = lookup(var.vpc_config, "vpc_id", null)
      subnets            = lookup(var.vpc_config, "subnets", null)
      security_group_ids = lookup(var.vpc_config, "security_group_ids", null)
    }
  }

  dynamic "logs_config" {
    for_each = length(var.logs_config) > 0 ? [""] : []
    content {
      dynamic "cloudwatch_logs" {
        for_each = contains(keys(var.logs_config), "cloudwatch_logs") ? { key = var.logs_config["cloudwatch_logs"] } : {}
        content {
          status      = lookup(cloudwatch_logs.value, "status", null)
          group_name  = lookup(cloudwatch_logs.value, "group_name", null)
          stream_name = lookup(cloudwatch_logs.value, "stream_name", null)
        }
      }

      dynamic "s3_logs" {
        for_each = contains(keys(var.logs_config), "s3_logs") ? { key = var.logs_config["s3_logs"] } : {}
        content {
          status              = lookup(s3_logs.value, "status", null)
          location            = lookup(s3_logs.value, "location", null)
          encryption_disabled = lookup(s3_logs.value, "encryption_disabled", null)
        }
      }
    }
  }
}
